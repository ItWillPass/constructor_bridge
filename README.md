# **Разводной мост**

### **Паттерны проектирования, выбранные для реализации:**
1. Порождающий паттерн — Фабричный метод  (Factory)
2. Структурный паттерн — Фасад (Facade)
3. Поведенческий паттерн — Состояние (state)

<br>

# **Реализация на Java**

## **Фабричный метод  (Factory)**

Используется, когда у нас есть суперкласс с несколькими подклассами и на основе ввода, нам нужно вернуть один из подкласса. Класс не знает какого типа объект он должен создать. Объекты создаются в зависимости от входящих данных.

Используется в классе **Factory**:
1. **Vehicle** (Абстрактный класс для создания транспортных средств)
2. **Car** (Класс для создания автомобилей)
3. **Ship** (Класс для создания кораблей)
4. **VehicleFactory** (Фабрика для создания транспортных средств)

<br>

## **Фасад (Facade)**

Скрывает сложную систему классов приводя все вызовы к одному объекту. Помещает вызов нескольких сложных объектов в один объект. 
Используется в классе **Facade**: 
1. **TrafficLights** (Класс, представляющий светофор)
2. **Bridge** (Класс, представляющий раздвижной мост)
3. **BridgeFacade** (Фасад для управления раздвижным мостом)

<br>

## **Состояние (State)**

Позволяет объекту изменять свое поведение в зависимости от его состояния.
Используется в классе **State**:
1. **BridgeState** (Интерфейс для состояний раздвижного моста)
2. **OpenState** (Класс, представляющий состояние "открыт")
3. **ClosedState** (Класс, представляющий состояние "закрыт")
4. **BridgeContext** (Класс, управляющий состоянием раздвижного моста)


