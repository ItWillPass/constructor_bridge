package Code;

class TrafficLights {
    void turnRed() {
        System.out.println("Traffic lights turn red.");
    }

    void turnGreen() {
        System.out.println("Traffic lights turn green.");
    }
}

class Bridge {
    void open() {
        System.out.println("Opening the bridge.");
    }

    void close() {
        System.out.println("Closing the bridge.");
    }
}

class BridgeFacade {
    private TrafficLights trafficLights;
    private Bridge bridge;

    BridgeFacade() {
        trafficLights = new TrafficLights();
        bridge = new Bridge();
    }

    void openBridge() {
        trafficLights.turnRed();
        bridge.open();
    }

    void closeBridge() {
        bridge.close();
        trafficLights.turnGreen();
    }
}