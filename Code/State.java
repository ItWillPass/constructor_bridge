package Code;

interface BridgeState {
    void handleState(BridgeContext context);
}

class OpenState implements BridgeState {
    @Override
    public void handleState(BridgeContext context) {
        System.out.println("Bridge is already open.");
    }
}

class ClosedState implements BridgeState {
    @Override
    public void handleState(BridgeContext context) {
        System.out.println("Closing the bridge.");
        context.setState(new OpenState());
    }
}

class BridgeContext {
    private BridgeState state;

    BridgeContext() {
        state = new ClosedState();
    }

    void setState(BridgeState state) {
        this.state = state;
    }

    void request() {
        state.handleState(this);
    }
}
