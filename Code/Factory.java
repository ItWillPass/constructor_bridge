package Code;

abstract class Vehicle {
    abstract void move();
}

class Car extends Vehicle {
    @Override
    void move() {
        System.out.println("Car is moving.");
    }
}

class Ship extends Vehicle {
    @Override
    void move() {
        System.out.println("Ship is sailing.");
    }
}

class VehicleFactory {
    Vehicle createVehicle(String type) {
        if (type.equalsIgnoreCase("Car")) {
            return new Car();
        } else if (type.equalsIgnoreCase("Ship")) {
            return new Ship();
        }
        return null;
    }
}
