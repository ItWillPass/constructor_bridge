package Code;

public class Main {
    public static void main(String[] args) {
        VehicleFactory vehicleFactory = new VehicleFactory();
        Vehicle car = vehicleFactory.createVehicle("Car");
        Vehicle ship = vehicleFactory.createVehicle("Ship");

        BridgeFacade bridgeFacade = new BridgeFacade();
        BridgeContext bridgeContext = new BridgeContext();

        bridgeFacade.openBridge();
        ship.move();
        bridgeFacade.closeBridge();
        car.move();
    }
}
