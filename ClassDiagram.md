```plantuml
@startuml
abstract class Vehicle {
    {abstract} void move()
}
class Car {
    + move()
}
class Ship {
    + move()
}
class VehicleFactory {
    + createVehicle(String type)
}
class TrafficLights {
    + turnRed()
    + turnGreen()
}
class Bridge {
    + open()
    + close()
}
class BridgeFacade {
    - trafficLights: TrafficLights
    - bridge: Bridge
    + openBridge()
    + closeBridge()
}
interface BridgeState {
    {abstract} + handleState(BridgeContext context)
}
class OpenState {
    + handleState(BridgeContext context)
}
class ClosedState {
    + handleState(BridgeContext context)
}
class BridgeContext {
    - state: BridgeState
    + setState(BridgeState state)
    + request()
}
Vehicle <|-- Car
Vehicle <|-- Ship
BridgeState <|.. OpenState
BridgeState <|.. ClosedState
VehicleFactory --> Vehicle
BridgeFacade --> TrafficLights
BridgeFacade --> Bridge
BridgeContext --> BridgeState
BridgeContext --> BridgeFacade
@enduml
```
